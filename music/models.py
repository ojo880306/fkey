from django.db import models

# Create your models here.
class Singer(models.Model) :
    object = models.Manager()
    #ID = models.AutoField(auto_created=True)
    sigrid = models.CharField(max_length=20,primary_key=True)
    sigrname = models.CharField(max_length=30,default='jj')
    sigrbirthday = models.DateField(default='2010-11-11') 
    sigremail = models.EmailField(default='joe@joe.com')
    

class Music(models.Model) :
    object = models.Manager()
    sgname = models.CharField(max_length=30)
    sgdate = models.DateField(auto_now_add=True)
    singer = models.ForeignKey(
        Singer,
        related_name='musicall',
        on_delete=models.CASCADE
    ) 
    def __str__(self):
        return self.sgname


class usernormal(models.Model) :
    object = models.Manager()
    #ID = models.AutoField(auto_created=True)
    userid = models.CharField(max_length=20,primary_key=True)
    username = models.CharField(max_length=30,default='jj')
    usertimecerat = models.DateTimeField(auto_now_add=True)
    useremail = models.EmailField(default='joe@joe.com')
    password = models.CharField(max_length=100,)

class Mood(models.Model):
    status = models.CharField(max_length=10, null=False)

    def __unicode__(self):
        return self.status    

class Post(models.Model):
    mood = models.ForeignKey('Mood', on_delete=models.CASCADE)
    nickname = models.CharField(max_length=10, default='不願意透漏身份的人')
    message = models.TextField(null=False)
    del_pass = models.CharField(max_length=10)
    pub_time = models.DateTimeField(auto_now=True)
    enabled = models.BooleanField(default=False)
    def __unicode__(self):
        return self.message

    
 






'''
class Reporter(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

class Article(models.Model):
    headline = models.CharField(max_length=100)
    pub_date = models.DateField()
    reporter = models.ForeignKey(
        Reporter,
        related_name='articles',
        on_delete=models.CASCADE
    )

    # reporter = models.ForeignKey(
    #     Reporter,
    #     on_delete=models.CASCADE)

    def __str__(self):
        return self.headline'
'''        
