from rest_framework import serializers
from music.models import Music,Singer,usernormal


class MusicSerializer(serializers.ModelSerializer):
   
    
    class Meta:
        model = Music
        fields = '__all__'
        #fields = ('id', 'song', 'singer', 'last_modify_date', 'created')

class SingerSerializer(serializers.ModelSerializer):
    #musicall =  serializers.StringRelatedField(many=True, read_only=True)    抓名字
    #musicall =  serializers.ReadOnlyField()
    musicall =  serializers.SlugRelatedField(                       #選擇抓哪一個
        many=True,
        read_only=True,
        slug_field='sgname'
     )



    class Meta:
        model = Singer
        fields = '__all__'
        #fields = ('id', 'song', 'singer', 'last_modify_date', 'created')      



class usernormalSerializer(serializers.ModelSerializer):
    class Meta:
        model = usernormal
        #fields = '__all__'
        fields = ('username', 'usertimecerat', 'useremail','password','userid')        

