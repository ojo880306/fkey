# Generated by Django 2.1.4 on 2019-01-11 15:29

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Music',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sgname', models.CharField(max_length=30)),
                ('sgdate', models.DateField(auto_now_add=True)),
            ],
            managers=[
                ('object', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Singer',
            fields=[
                ('sigrid', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('sigrname', models.CharField(default='jj', max_length=30)),
                ('sigrbirthday', models.DateField(default='2010-11-11')),
                ('sigremail', models.EmailField(default='joe@joe.com', max_length=254)),
            ],
            managers=[
                ('object', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AddField(
            model_name='music',
            name='singer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='singers', to='music.Singer'),
        ),
    ]
