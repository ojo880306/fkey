# Generated by Django 2.1.4 on 2019-01-14 03:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0003_auto_20190114_1144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='music',
            name='musics',
        ),
        migrations.AddField(
            model_name='music',
            name='singer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='musicss', to='music.Singer'),
            preserve_default=False,
        ),
    ]
