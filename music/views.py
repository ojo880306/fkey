from django.shortcuts import render
from music.models import Music,Singer,usernormal
from music.serializers import MusicSerializer,SingerSerializer,usernormalSerializer
from django.db.models import Q

from rest_framework import viewsets


# Create your views here.
class MusicViewSet(viewsets.ModelViewSet):
    queryset = Music.object.all()
    serializer_class = MusicSerializer

    def get_queryset(self):
            queryset = Music.object.all()
            query = self.request.query_params.get('Q',None)
            if query:
                queryset = queryset.filter(
                    Q(sgname=query) |
                    Q(singer=query)
                    ).distinct()
            return queryset   


# Create your views here.
class SingerViewSet(viewsets.ModelViewSet):
    queryset = Singer.object.all()
    serializer_class = SingerSerializer

class usernormalViewSet(viewsets.ModelViewSet):
    queryset = usernormal.object.all()
    serializer_class = usernormalSerializer

def new2(request):
    #students = student.objects.all() #依據id欄位遞減排序顯示所有資料
    return render(request,'new2.html',locals()) 
    '''
class getall(viewsets):
    template_name = 'core/signup.html'
    form_class = UserCreationForm


'''


